#!/usr/bin/env ruby

require 'rubygems'
require 'active_resource'

class Issue < ActiveResource::Base
  self.site = ''
  self.headers['X-Redmine-API-Key'] = ''
  self.format = :xml
end

class Merge_requests < ActiveResource::Base
  self.site = ''
  self.headers['PRIVATE-TOKEN'] = ''
end

puts 'Redmine Issues'
puts ' WIP & Closed'
issues = Issue.find(:all, :params => { :project_id => '1', :updated_on => '><2018-01-01|2018-01-01', :status_id => '*'  })
issues.each do |issue|
    # if issue.fixed_version?  and issue.fixed_version.name == '2018-08-13'
    puts "\n#{issue.subject} : #{issue.id}"
    puts "  Status : #{issue.status.name}"
    #puts "  by #{issue.author.name}"
    puts "  Version : #{issue.fixed_version.name}" if issue.fixed_version?
    puts "  Last update : #{issue.updated_on}" 
    # end
end
puts 'GitLab MR'
merge_requests = Merge_requests.find(:all, :params => { :scope => 'all', :state => 'all', :updated_before => '2018-01-01', :updated_after => '2018-01-01', :order_by => 'updated_at' })
merge_requests.each do |merge_request|
  puts "\n#{merge_request.title} : #{merge_request.iid}"
  puts "  Branch : #{merge_request.source_branch}"
  puts "  Status : #{merge_request.state}"
  puts "  Milestone : #{merge_request.milestone.title}" if defined? merge_request.milestone.title
  puts "  Last Update : #{merge_request.updated_at}" 
end

