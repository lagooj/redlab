# redlab

*Playing around with (quick & dirty) ruby and gitlab/redmine Rest API*

## redlab.rb: 
I use it to sync roadmap and milestone name and due date in redmine and gitlab.

## rl_list.rb: 
Prints out all Merge Requests and Redmine Issues for a given timerange.
