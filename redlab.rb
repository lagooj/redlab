#!/usr/bin/env ruby
require 'rubygems'
require 'active_resource'
require 'optparse'

class Version < ActiveResource::Base
  self.site = ''
  self.headers['X-Redmine-API-Key'] = ''
  self.format = :xml
end

class Milestones < ActiveResource::Base
  self.site = ''
  self.headers['PRIVATE-TOKEN'] = ''
end

options = {}
OptionParser.new do |opts|
  opts.banner = "Usage: redlab.rb params"
  opts.on("--name [TEXT]", String, :REQUIRED, "REQUIRED Set version/milestone name") do |n|
    options[:name] = n
  end
  opts.on("--due-date [DATE] YYYY-MM-DD", "Set due_date for the current version/milestone set with -n") do |d|
    options[:duedate] = d
  end
end.parse!
new_v = Version.new( :name => options[:name] , :due_date => options[:duedate] )
new_v.save

new_m = Milestones.new( :id => '01', :title => options[:name], :due_date => options[:duedate] )
new_m.save
